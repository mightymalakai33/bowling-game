# Bowling Game

Simple bowling game based on an unrelated childhood game of mine.


10 active bowlers at a time
10 competitions in a season
a season lasts 2 weeks (m-f for 2 weeks, 10 days)
each season a winner is decided
the 3 lowest bowlers are dropped

50 bowlers in reserves ready to compete for the 3 spots
bowlers can't compete for entry for 6 seasons after they've been dropped

bowlers are forced to retire after 20 seasons in the league

use real dates to add some more realism to it, starting in 1958





bowling app competition design

X bowlers per day (10 in this case)
X competitions per season (10 also)
X bowlers dropped at end of season and rotated in (2 for now)
X number of bowlers in reserves, possibly increasing over time
bowlers retire after certain criteria

dropped bowlers can't compete for X seasons (not sure what this will be set to )

each day points are earned based on position, 1 for lowest X for highest (10)

X bonus points for bowling a 300 game (5)
X bonus points for highest game bowled in a season (3)
X - 1 bonus points for next highest game bowled (2)
X highested bowled games earn bonus points (3)
highest bowled game bonus points can only be earned once per bowler. For example, if a bowler has 2 300's and come in 1st and 2nd they will only earn 3 points.

there will be weekly, seasonal and all-time standings

## settings ##
backup location where all data is stored
need a save format



## code ##

# Data #
class Score (bowler ID, day ID, season ID, score, season point value)
class Bowler (stores all scores for a bowler) (has methods for getting best score, day, season, anything)
class Week (stores scores for a single day) (has methods for getting points totals and leaders, etc)
class Season (stores scores for an entire season, so an array of X Week class instances) (has methods for getting points totals and leaders, stuff like that)

# Logic #

class BowlerAssignment (responsible for cycling in/out bowlers between seasons)
class WeekBuilder
class SeasonBuilder
