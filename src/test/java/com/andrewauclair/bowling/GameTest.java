package com.andrewauclair.bowling;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.andrewauclair.bowling.TestUtils.bowler;
import static com.andrewauclair.bowling.TestUtils.bowlers;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class GameTest {
	@Test
	void game_starts_with_predefined_stats() {
		Game game = new Game(Game.DEFAULT_BOWLERS_PER_SEASON, Game.DEFAULT_WEEKS_PER_SEASON, Game.DEFAULT_RESERVE_BOWLERS, Game.DEFAULT_BOWLERS_TO_CUT, Game.DEFAULT_WAIT_SEASON_COUNT, Game.DEFAULT_SEASONS_TILL_RETIREMENT);
		
		assertThat(game.bowlers()).isEmpty();
		
		List<Bowler> expectedReserves = IntStream.range(1, 61).boxed().map(Bowler::new).collect(Collectors.toList());
		
		assertThat(game.reserveBowlers()).containsExactlyElementsOf(expectedReserves);
	}
	
	@Test
	void adding_game_adds_it_to_active_season() {
		Game game = new Game(Game.DEFAULT_BOWLERS_PER_SEASON, Game.DEFAULT_WEEKS_PER_SEASON, Game.DEFAULT_RESERVE_BOWLERS, Game.DEFAULT_BOWLERS_TO_CUT, Game.DEFAULT_WAIT_SEASON_COUNT, Game.DEFAULT_SEASONS_TILL_RETIREMENT);

		Season season = new Season(game, 1, 10, 10, 3);
		
		season.addBowler(bowler(1));
		
		season.addScore(new Score(bowler(1), 200));
		
		assertThat(season.activeWeek().scores()).containsOnly(new Score(bowler(1), 200));
	}

	@Test
	void competition_winners_are_added_to_season_and_remove_from_reserves() {
		Game game = new Game(Game.DEFAULT_BOWLERS_PER_SEASON, Game.DEFAULT_WEEKS_PER_SEASON, Game.DEFAULT_RESERVE_BOWLERS, Game.DEFAULT_BOWLERS_TO_CUT, Game.DEFAULT_WAIT_SEASON_COUNT, Game.DEFAULT_SEASONS_TILL_RETIREMENT);

		// need to skip competition part
		for (int i = 1; i <= Game.DEFAULT_BOWLERS_PER_SEASON; i++) {
			game.addScore(bowler(i), 100);
		}

		for (int i = 1; i <= Game.DEFAULT_RESERVE_BOWLERS; i++) {
			game.addScore(bowler(i + Game.DEFAULT_BOWLERS_PER_SEASON), 1);
		}

		assertThat(game.bowlers()).containsOnly(bowlers(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)));

		List<Integer> expectedReserves = IntStream.range(11, 61).boxed().collect(Collectors.toList());

		assertThat(game.reserveBowlers()).containsExactlyElementsOf(expectedReserves);
	}

	@Test
	void start_new_competition_when_season_is_complete() {
		Game game = new Game(Game.DEFAULT_BOWLERS_PER_SEASON, Game.DEFAULT_WEEKS_PER_SEASON, Game.DEFAULT_RESERVE_BOWLERS, Game.DEFAULT_BOWLERS_TO_CUT, Game.DEFAULT_WAIT_SEASON_COUNT, Game.DEFAULT_SEASONS_TILL_RETIREMENT);
		
		// need to skip competition part
		for (int i = 1; i <= Game.DEFAULT_BOWLERS_PER_SEASON; i++) {
			game.addScore(i, 100);
		}
		
		for (int i = 1; i <= Game.DEFAULT_RESERVE_BOWLERS; i++) {
			game.addScore(bowler(i + Game.DEFAULT_BOWLERS_PER_SEASON), 1);
		}
		
		Season season1 = new Season(game, 1, 10, 10, 3);
		
		for (int i = 1; i <= Game.DEFAULT_BOWLERS_PER_SEASON; i++) {
			season1.addBowler(bowler(i));
		}
		
		for (int i = 0; i < Game.DEFAULT_WEEKS_PER_SEASON; i++) {
			for (int j = 0; j < Game.DEFAULT_BOWLERS_PER_SEASON; j++) {
				game.addScore(bowler(j + 1), 200);
				season1.addScore(new Score(bowler(j + 1), 200));
			}
		}
		
		assertTrue(game.isCompetition());
		
		Comp competition = game.getCompetition();
		
		assertEquals(3, competition.cutSize());
		assertEquals(50, competition.totalBowlers()); // reserve bowlers + bowlers per season to start
		
		assertTrue(game.isCompetition());
	}

	@Test
	void decrease_waiting_period_for_bowlers_after_season() {
		Game game = new Game(Game.DEFAULT_BOWLERS_PER_SEASON, Game.DEFAULT_WEEKS_PER_SEASON, Game.DEFAULT_RESERVE_BOWLERS, Game.DEFAULT_BOWLERS_TO_CUT, Game.DEFAULT_WAIT_SEASON_COUNT, Game.DEFAULT_SEASONS_TILL_RETIREMENT);
		game.setWaitingBowler(100, 6);

		// need to skip competition part
		for (int i = 1; i <= Game.DEFAULT_BOWLERS_PER_SEASON; i++) {
			game.addScore(i, 100);
		}

		for (int i = 1; i <= Game.DEFAULT_RESERVE_BOWLERS; i++) {
			game.addScore(i + Game.DEFAULT_BOWLERS_PER_SEASON, 1);
		}

		Season season1 = new Season(1);

		for (int i = 1; i <= Game.DEFAULT_BOWLERS_PER_SEASON; i++) {
			season1.addBowler(i);
		}

		for (int i = 0; i < Game.DEFAULT_WEEKS_PER_SEASON; i++) {
			for (int j = 0; j < Game.DEFAULT_BOWLERS_PER_SEASON; j++) {
				game.addScore(j + 1, 200);
				season1.addScore(new Score(j + 1, 200));
			}
		}

		assertTrue(game.isCompetition());

		Comp competition = game.getCompetition();

		assertEquals(3, competition.cutSize());
		assertEquals(50, competition.totalBowlers()); // reserve bowlers + bowlers per season to start

		assertThat(game.waitingBowlers()).containsEntry(100, 5);
		assertThat(game.waitingBowlers()).doesNotContainEntry(100, 6);

		assertTrue(game.isCompetition());
	}

	@Test
	void add_back_waiting_bowlers_when_season_wait_is_0() {
		Game game = new Game(Game.DEFAULT_BOWLERS_PER_SEASON, Game.DEFAULT_WEEKS_PER_SEASON, Game.DEFAULT_RESERVE_BOWLERS, Game.DEFAULT_BOWLERS_TO_CUT, Game.DEFAULT_WAIT_SEASON_COUNT, Game.DEFAULT_SEASONS_TILL_RETIREMENT);
		game.setWaitingBowler(100, 1);

		// need to skip competition part
		for (int i = 1; i <= Game.DEFAULT_BOWLERS_PER_SEASON; i++) {
			game.addScore(i, 100);
		}

		for (int i = 1; i <= Game.DEFAULT_RESERVE_BOWLERS; i++) {
			game.addScore(i + Game.DEFAULT_BOWLERS_PER_SEASON, 1);
		}

		Season season1 = new Season(1);

		for (int i = 1; i <= Game.DEFAULT_BOWLERS_PER_SEASON; i++) {
			season1.addBowler(i);
		}

		for (int i = 0; i < Game.DEFAULT_WEEKS_PER_SEASON; i++) {
			for (int j = 0; j < Game.DEFAULT_BOWLERS_PER_SEASON; j++) {
				game.addScore(j + 1, 200);
				season1.addScore(new Score(j + 1, 200));
			}
		}

		assertTrue(game.isCompetition());

		Comp competition = game.getCompetition();

		assertEquals(3, competition.cutSize());
		assertEquals(51, competition.totalBowlers()); // reserve bowlers + bowlers per season to start

		assertThat(competition.getBowlers()).contains(100);
		assertThat(game.reserveBowlers()).contains(100);
		assertThat(game.waitingBowlers()).doesNotContainKey(100);

		assertTrue(game.isCompetition());
	}

	@Test
	void game_starts_in_competition_mode() {
		Game game = new Game(Game.DEFAULT_BOWLERS_PER_SEASON, Game.DEFAULT_WEEKS_PER_SEASON, Game.DEFAULT_RESERVE_BOWLERS, Game.DEFAULT_BOWLERS_TO_CUT, Game.DEFAULT_WAIT_SEASON_COUNT, Game.DEFAULT_SEASONS_TILL_RETIREMENT);
		
		assertTrue(game.isCompetition());
	}
	
	@Test
	void adding_a_score_to_game_during_competition_does_not_add_it_to_season() {
		Game game = new Game(Game.DEFAULT_BOWLERS_PER_SEASON, Game.DEFAULT_WEEKS_PER_SEASON, Game.DEFAULT_RESERVE_BOWLERS, Game.DEFAULT_BOWLERS_TO_CUT, Game.DEFAULT_WAIT_SEASON_COUNT, Game.DEFAULT_SEASONS_TILL_RETIREMENT);
		
		game.addScore(1, 9);
		
		Season season = new Season(0);
		
		assertEquals(season, game.activeSeason());
	}
}
