package com.andrewauclair.bowling;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static com.andrewauclair.bowling.TestUtils.bowler;
import static org.junit.jupiter.api.Assertions.*;

public class Comp_Test {
	@Test
	void has_bowlers_at_start() {
		Comp comp = new Comp(2, TestUtils.bowlers(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8)));
		
		assertEquals(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8), comp.getBowlers());
	}
	@Test
	void add_scores() {
		Comp comp = new Comp(2, TestUtils.bowlers(Arrays.asList(1, 2, 3, 4)));
		comp.addScore(bowler(1), 20);
		comp.addScore(bowler(2), 20);
		comp.addScore(bowler(3), 25);
		
		assertEquals(TestUtils.bowlers(Arrays.asList(25, 20, 20)), comp.getScores());
		assertEquals(Collections.singletonList(4), comp.getBowlers());
	}
	
	@Test
	void remaining_bowlers_will_work_out_scores() {
		Comp comp = new Comp(2, TestUtils.bowlers(Arrays.asList(1, 2, 3, 4, 5)));
		comp.addScore(bowler(1), 20);
		comp.addScore(bowler(2), 20);
		comp.addScore(bowler(3), 25);
		comp.addScore(bowler(4), 25);
		comp.addScore(bowler(5), 25);
		
		assertEquals(Collections.emptyList(), comp.getScores());
		assertEquals(TestUtils.bowlers(Arrays.asList(3, 4, 5)), comp.getBowlers());
	}
	
	@Test
	
	void not_complete() {
		Comp comp = new Comp(2, TestUtils.bowlers(Arrays.asList(1, 2, 3, 4, 5)));
		comp.addScore(bowler(1), 20);
		comp.addScore(bowler(2), 20);
		comp.addScore(bowler(3), 25);
		comp.addScore(bowler(4), 25);
		comp.addScore(bowler(5), 25);
		
		assertFalse(comp.isComplete());
	}
	
	@Test
	void when_not_complete__only_tied_bowlers_go_again__tie_for_first() {
		Comp comp = new Comp(2, TestUtils.bowlers(Arrays.asList(1, 2, 3, 4, 5)));
		comp.addScore(bowler(1), 20);
		comp.addScore(bowler(2), 20);
		comp.addScore(bowler(3), 25);
		comp.addScore(bowler(4), 25);
		comp.addScore(bowler(5), 25);
		
		assertEquals(TestUtils.bowlers(Arrays.asList(3, 4, 5)), comp.getBowlers());
	}
	
	@Test
	void when_not_complete__only_tied_bowlers_go_again__tie_for_last() {
		Comp comp = new Comp(2, TestUtils.bowlers(Arrays.asList(1, 2, 3, 4, 5)));
		comp.addScore(bowler(1), 20);
		comp.addScore(bowler(2), 20);
		comp.addScore(bowler(3), 20);
		comp.addScore(bowler(4), 20);
		comp.addScore(bowler(5), 25);
		
		assertEquals(TestUtils.bowlers(Arrays.asList(1, 2, 3, 4)), comp.getBowlers());
		
		comp.addScore(bowler(1), 19);
		comp.addScore(bowler(2), 19);
		comp.addScore(bowler(3), 24);
		comp.addScore(bowler(4), 20);
		
		assertEquals(TestUtils.bowlers(Arrays.asList(5, 3)), comp.getBowlers());
	}
	
	@Test
	void complete() {
		Comp comp = new Comp(2, TestUtils.bowlers(Arrays.asList(1, 2, 3, 4)));
		comp.addScore(bowler(1), 20);
		comp.addScore(bowler(2), 20);
		comp.addScore(bowler(3), 25);
		comp.addScore(bowler(4), 25);
		
		assertTrue(comp.isComplete());
		assertEquals(TestUtils.bowlers(Arrays.asList(3, 4)), comp.getBowlers());
	}
}
