package com.andrewauclair.bowling;

import java.util.List;
import java.util.stream.Collectors;

public class TestUtils {
	public static Bowler bowler(int id) {
		return new Bowler(id);
	}

	public static List<Bowler> bowlers(List<Integer> bowlers) {
		return bowlers.stream().map(Bowler::new).collect(Collectors.toList());
	}
}
