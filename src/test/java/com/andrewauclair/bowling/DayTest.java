package com.andrewauclair.bowling;

import org.junit.jupiter.api.Test;

import static com.andrewauclair.bowling.TestUtils.bowler;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class DayTest {
	@Test
	void create_a_week() {
		Day day = new Day(8, 10);
		
		assertEquals(8, day.weekID());
	}
	
	@Test
	void add_a_score() {
		Day day = new Day(1, 10);
		
		day.add(new Score(bowler(2), 220));
		
		assertThat(day.scores()).containsOnly(new Score(bowler(2), 220));
	}
	
	@Test
	void scores_are_sorted() {
		Day day = new Day(1, 10);
		
		day.add(new Score(bowler(1), 190));
		day.add(new Score(bowler(2), 220));
		day.add(new Score(bowler(3), 210));
		
		assertThat(day.scores()).containsExactly(
			new Score(bowler(2), 220),
			new Score(bowler(3), 210),
			new Score(bowler(1), 190)
		);
	}
	
	@Test
	void week_is_complete_after_10th_game() {
		Day day = new Day(1, 10);
		
		assertFalse(day.isComplete());
		
		day.add(new Score(bowler(1), 290));
		day.add(new Score(bowler(2), 280));
		day.add(new Score(bowler(3), 270));
		day.add(new Score(bowler(4), 260));
		day.add(new Score(bowler(5), 250));
		day.add(new Score(bowler(6), 240));
		day.add(new Score(bowler(7), 230));
		day.add(new Score(bowler(8), 220));
		day.add(new Score(bowler(9), 210));
		day.add(new Score(bowler(10), 200));
		
		assertTrue(day.isComplete());
		
		// throws an exception after complete, just a helper exception for later and prevents extra scores from being added
		assertThrows(RuntimeException.class, () -> day.add(new Score(bowler(11), 200)));
		
		assertThat(day.scores()).containsOnly(
			new Score(bowler(1), 290),
			new Score(bowler(2), 280),
			new Score(bowler(3), 270),
			new Score(bowler(4), 260),
			new Score(bowler(5), 250),
			new Score(bowler(6), 240),
			new Score(bowler(7), 230),
			new Score(bowler(8), 220),
			new Score(bowler(9), 210),
			new Score(bowler(10), 200)
		);
	}
	
	@Test
	void describe_the_week() {
		Day day = new Day(3, 10);
		
		day.add(new Score(bowler(7), 290));
		day.add(new Score(bowler(2), 280));
		day.add(new Score(bowler(5), 270));
		day.add(new Score(bowler(4), 260));
		day.add(new Score(bowler(3), 250));
		day.add(new Score(bowler(6), 240));
		day.add(new Score(bowler(8), 230));
		day.add(new Score(bowler(1), 220));
		day.add(new Score(bowler(9), 210));
		day.add(new Score(bowler(10), 200));
		
		assertEquals("Season 2, Week 3\n" +
						"\n" +
						" 7  290\n" +
						" 2  280\n" +
						" 5  270\n" +
						" 4  260\n" +
						" 3  250\n" +
						" 6  240\n" +
						" 8  230\n" +
						" 1  220\n" +
						" 9  210\n" +
						"10  200\n\n",
				day.description(2));
	}
}
