package com.andrewauclair.bowling;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class SeasonTest {
	@Test
	void create() {
		Season season = new Season(2);
		
		assertEquals(2, season.seasonID());
		assertEquals(new Day(1), season.activeWeek());
	}
	
	@Test
	void season_has_10_bowlers() {
		Season season = new Season(2);
		
		season.addBowler(1);
		season.addBowler(2);
		season.addBowler(3);
		
		assertThat(season.bowlers()).containsOnly(1, 2, 3);
	}
	
	@Test
	void adding_game_to_season_adds_it_to_active_week() {
		Season season = new Season(1);
		
		season.addBowler(1);
		
		season.addScore(new Score(1, 200));
		
		assertThat(season.activeWeek().scores()).contains(new Score(1, 200));
	}
	
	@Test
	void after_week_is_complete_a_new_week_is_started() {
		Season season = new Season(1);
		
		Day day1 = new Day(1);
		
		for (int i = 0; i < 10; i++) {
			season.addScore(new Score(i + 1, 200));
			day1.add(new Score(i + 1, 200));
		}
		
		assertThat(season.activeWeek().scores()).isEmpty();
		assertEquals(2, season.activeWeek().weekID());
		
		assertThat(season.weeks()).contains(
				day1,
				new Day(2)
		);
	}
	
	@Test
	void season_is_not_complete_when_final_week_is_in_progress() {
		Season season = new Season(1);
		
		assertFalse(season.isComplete());
		
		Day[] days = new Day[Game.WEEKS_PER_SEASON];
		
		for (int i = 0; i < Game.WEEKS_PER_SEASON - 1; i++) {
			days[i] = new Day(i + 1);
			
			for (int j = 0; j < Game.BOWLERS_PER_SEASON; j++) {
				season.addScore(new Score(j + 1, 200));
				days[i].add(new Score(j + 1, 200));
			}
		}
		
		season.addScore(new Score(4, 200));
		
		assertFalse(season.isComplete());
	}
	
	@Test
	void season_is_complete_after_final_week() {
		Season season = new Season(1);
		
		assertFalse(season.isComplete());
		
		Day[] days = new Day[Game.WEEKS_PER_SEASON];
		
		for (int i = 0; i < Game.WEEKS_PER_SEASON; i++) {
			days[i] = new Day(i + 1);
			
			for (int j = 0; j < Game.BOWLERS_PER_SEASON; j++) {
				season.addScore(new Score(j + 1, 200));
				days[i].add(new Score(j + 1, 200));
			}
		}
		
		assertTrue(season.isComplete());
		assertThat(season.weeks()).containsOnly(days);
		
		// throws an exception after complete, just a helper exception for later and prevents extra scores from being added
		assertThrows(RuntimeException.class, () -> season.addScore(new Score(11, 200)));
	}
	
	@Test
	void describe_season_with_1_week_of_scores() {
		Season season = new Season(2);
		
		for (int i = 1; i <= Game.BOWLERS_PER_SEASON; i++) {
			season.addBowler(i);
		}
		
		season.addScore(new Score(7, 290));
		season.addScore(new Score(2, 280));
		season.addScore(new Score(5, 270));
		season.addScore(new Score(4, 260));
		season.addScore(new Score(3, 250));
		season.addScore(new Score(6, 240));
		season.addScore(new Score(8, 230));
		season.addScore(new Score(1, 220));
		season.addScore(new Score(9, 210));
		season.addScore(new Score(10, 200));
		
		assertEquals("Season 2 Standings, Week 1 / 10\n" +
						"Pos   Bowler  Points  Bonus  W   S   High  Best  Wins\n" +
						"# 1#       7      13      3  --  --   290     1     1\n" +
						"# 2#       2      11      2  --  --   280     2     0\n" +
						"# 3#       5       9      1  --  --   270     3     0\n" +
						"# 4#       4       7         --  --   260     4     0\n" +
						"# 5#       3       6         --  --   250     5     0\n" +
						"# 6#       6       5         --  --   240     6     0\n" +
						"# 7#       8       4         --  --   230     7     0\n" +
						"# 8#       1       3         --  --   220     8     0\n" +
						"# 9#       9       2         --  --   210     9     0\n" +
						"#10#      10       1         --  --   200    10     0\n",
				season.describe());
	}
	
	private Score score(int bowlerID, int score) {
		return new Score(bowlerID, score);
	}
	
	private void addScores(Season season, List<Score> scores) {
		for (Score score : scores) {
			season.addScore(score);
		}
	}
	
	@Test
	void describe_season_with_5_weeks_of_scores() {
		Season season = new Season(2);
		
		for (int i = 1; i <= Game.BOWLERS_PER_SEASON; i++) {
			season.addBowler(i);
		}
		
		// Week 1
		addScores(season, Arrays.asList(
				score(2, 300), score(10, 278), score(3, 268),
				score(4, 242), score(6, 223), score(7, 217),
				score(8, 215), score(9, 212), score(5, 201),
				score(1, 189)
		));
		
		// Week 2
		addScores(season, Arrays.asList(
				score(7, 267), score(1, 267), score(3, 246),
				score(8, 245), score(2, 245), score(4, 238),
				score(9, 237), score(6, 235), score(10, 203),
				score(5, 190)
		));
		
		// Week 3
		addScores(season, Arrays.asList(
				score(10, 290), score(6, 280), score(7, 278),
				score(5, 268), score(9, 244), score(1, 235),
				score(3, 234), score(4, 228), score(8, 208),
				score(2, 165)
		));
		
		// Week 4
		addScores(season, Arrays.asList(
				score(6, 279), score(1, 275), score(9, 267),
				score(8, 265), score(4, 257), score(10, 247),
				score(5, 247), score(2, 238), score(3, 221),
				score(7, 215)
		));
		
//		 Week 5
		addScores(season, Arrays.asList(
				score(7, 268), score(5, 259), score(10, 257),
				score(1, 256), score(3, 254), score(2, 238),
				score(6, 235), score(9, 217), score(4, 211),
				score(8, 198)));
		
		
		// b   points    bonus    w    s  high  best wins
		//10     36       2      +2     +1     290     1    1
		// 2     34       8      -1     -1     300     1    1
		// 7     34              +2     +3     268    1    2
		// 6     33       1      -2     +1     280    1    1
		// 1     32              -1     +5     267    1    1
		// 3     28              --     -3     268     3    0
		// 5     24              +3     +2     268    2    0
		// 9     24              -1     --     267    3    0
		// 8     21              -1     -2     265    4    0
		// 4     20              -1     -6     257     4    0
		assertEquals("Season 2 Standings, Week 5 / 10\n" +
						"Pos   Bowler  Points  Bonus  W   S   High  Best  Wins\n" +
						"# 1#      10      36      2  +1  +1   290     1     1\n" +
						"# 2#       7      34         +3  +4   278     1     2\n" +
						"# 3#       6      33      1  -2  +2   280     1     1\n" +
						"# 4#       2      32      6  -1  -3   300     1     1\n" +
						"# 5#       1      32         -1  +5   275     1     1\n" +
						"# 6#       3      28         --  -3   268     3     0\n" +
						"# 7#       5      24         +3  +2   268     2     0\n" +
						"# 8#       9      24         -1  --   267     3     0\n" +
						"# 9#       4      23         -1  -5   257     4     0\n" +
						"#10#       8      21         -1  -3   265     4     0\n",
				season.describe());
	}
	
	@Test
	void cut_bowlers() {
		Season season = new Season(2);
		
		for (int i = 1; i <= Game.BOWLERS_PER_SEASON; i++) {
			season.addBowler(i);
		}
		
		// Week 1
		addScores(season, Arrays.asList(
				score(2, 300), score(10, 278), score(3, 268),
				score(4, 242), score(6, 223), score(7, 217),
				score(8, 215), score(9, 212), score(5, 201),
				score(1, 189)
		));
		
		// Week 2
		addScores(season, Arrays.asList(
				score(7, 267), score(1, 267), score(3, 246),
				score(8, 245), score(2, 245), score(4, 238),
				score(9, 237), score(6, 235), score(10, 203),
				score(5, 190)
		));
		
		// Week 3
		addScores(season, Arrays.asList(
				score(10, 290), score(6, 280), score(7, 278),
				score(5, 268), score(9, 244), score(1, 235),
				score(3, 234), score(4, 228), score(8, 208),
				score(2, 165)
		));
		
		// Week 4
		addScores(season, Arrays.asList(
				score(6, 279), score(1, 275), score(9, 267),
				score(8, 265), score(4, 257), score(10, 247),
				score(5, 247), score(2, 238), score(3, 221),
				score(7, 215)
		));

//		 Week 5
		addScores(season, Arrays.asList(
				score(7, 268), score(5, 259), score(10, 257),
				score(1, 256), score(3, 254), score(2, 238),
				score(6, 235), score(9, 217), score(4, 211),
				score(8, 198)));
		
		assertThat(season.cutBowlers()).containsOnly(9, 4, 8);
	}
}
