package com.andrewauclair.bowling;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ScoreTest {
	@Test
	void create_score() {
		Score score = new Score(5, 220);
		
		assertEquals(5, score.bowlerID());
		assertEquals(220, score.score());
	}
	
	@Test
	void equals() {
		EqualsVerifier.forClass(Score.class).verify();
	}
}
