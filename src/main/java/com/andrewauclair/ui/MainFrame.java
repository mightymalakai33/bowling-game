package com.andrewauclair.ui;

import com.andrewauclair.bowling.Game;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class MainFrame extends JFrame {

	JButton back_button = new JButton("back");
	JButton forward_button = new JButton("forward");
	JButton enter_score_button = new JButton("Enter");

	DefaultTableModel seasons_model = new DefaultTableModel(new Object[] { "", ""}, 0);
	JTable seasons_list = new JTable(seasons_model);

	DefaultTableModel days_model = new DefaultTableModel(new Object[] {"", ""}, 0);
	JTable days_list = new JTable(days_model);

	public MainFrame(Game game) {
		JPanel options_panel = new JPanel(new FlowLayout());
		options_panel.add(back_button);
		options_panel.add(forward_button);
		options_panel.add(enter_score_button);

		enter_score_button.addActionListener(e -> new EnterScoreDialog().setVisible(true));

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridy = 0;

		setLayout(new GridBagLayout());

		add(options_panel, gbc);
		gbc.gridy++;

		add(new JScrollPane(seasons_list), gbc);

	}

	public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

		MainFrame frame = new MainFrame(null);
		frame.setSize(500, 500);

		frame.setVisible(true);
	}
}
