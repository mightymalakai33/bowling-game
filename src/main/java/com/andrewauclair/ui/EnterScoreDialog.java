package com.andrewauclair.ui;

import javax.swing.*;
import java.awt.*;

public class EnterScoreDialog extends JFrame {
	JTextField enter_score = new JTextField(3);


	public EnterScoreDialog() {
		setSize(500, 500);

		JPanel controls = new JPanel(new FlowLayout());

		controls.add(new JLabel("Enter Score: "));
		controls.add(enter_score);

		add(controls);

		// show competition top ~10 if competition

		// if season, show the current day on the left and the season standings on the right
	}
}
