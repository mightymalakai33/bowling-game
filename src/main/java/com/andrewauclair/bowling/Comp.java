package com.andrewauclair.bowling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Comp {
	public int cutSize() {
		return cutSize;
	}
	
	public int totalBowlers() {
		return totalBowlers;
	}
	
	public List<Score> getFinalScores() {
		return Collections.unmodifiableList(finalScores);
	}
	
	public static class Score {
		public final Bowler bowler;
		public final int score;
		
		public Score(Bowler bowler, int score) {
			this.bowler = bowler;
			this.score = score;
		}
		
		@Override
		public String toString() {
			return bowler.id + " (" + score + ")";
		}
	}
	
	private final List<Score> finalScores = new ArrayList<>();
	
	private final List<Score> carryOverScores = new ArrayList<>();
	
	private final List<Score> scores = new ArrayList<>();
	
	private final List<Bowler> bowlers = new ArrayList<>();
	
	private final int cutSize;
	private final int totalBowlers;
	
	private boolean complete = false;
	
	public Comp(int cutSize, List<Bowler> bowlers) {
		this.cutSize = cutSize;
		this.totalBowlers = bowlers.size();
		
		this.bowlers.addAll(bowlers);
	}
	
	public void addScore(Bowler bowler, int score) {
		scores.add(new Score(bowler, score));
		
		bowlers.removeIf(b -> b.id == bowler.id);

		if (bowlers.size() == 0) {
			trim();
		}
	}
	
	public void trim() {
		scores.addAll(carryOverScores);
		carryOverScores.clear();
		
		scores.sort(((Comparator<Score>) (o1, o2) -> {
			return Integer.compare(o1.score, o2.score);
		}).reversed());
		
		for (int i = scores.size() - 1; i >= cutSize; i--) {
			if (scores.get(i).score < scores.get(cutSize - 1).score) {
				Score remove = scores.remove(i);
				finalScores.add(remove);
			}
		}
		
		if (scores.size() > cutSize) {
			Score cutScore = scores.get(cutSize - 1);
			
			for (Score score : scores) {
				if (score.score > cutScore.score) {
					carryOverScores.add(score);
				}
			}
			
			for (Score carryOverScore : carryOverScores) {
				scores.remove(carryOverScore);
			}
			
			for (Score score : scores) {
				bowlers.add(score.bowler);
			}
			
			scores.clear();
		}
		else {
			for (Score score : scores) {
				bowlers.add(score.bowler);
				finalScores.add(score);
			}
			
			complete = true;
		}
	}
	
	public boolean isComplete() {
		return complete;
	}
	
	public List<Integer> getScores() {
		scores.sort(((Comparator<Score>) (o1, o2) -> {
			return Integer.compare(o1.score, o2.score);
		}).reversed());
		
		return scores.stream()
				.map(score -> score.score)
				.collect(Collectors.toList());
	}
	
	public List<Bowler> getBowlers() {
		return bowlers;
	}
	
	public List<Bowler> lockedInBowlers() {
		return carryOverScores.stream()
				.map(score -> score.bowler)
				.collect(Collectors.toList());
	}
}
