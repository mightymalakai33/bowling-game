package com.andrewauclair.bowling;

import java.util.*;
import java.util.stream.Collectors;

import static com.andrewauclair.bowling.ConsoleTable.Alignment.RIGHT;

public class Season {
	private final Game game;
	private final int seasonID;
	
	private final List<Bowler> bowlers = new ArrayList<>();
	
	private final List<Day> days = new ArrayList<>();
	
	private Day activeDay;
	
	private final int weeksPerSeason;
	private final int bowlersToCut;
	private final int bowlersPerSeason;
	
	public Season(Game game, int seasonID, int bowlersPerSeason, int weeksPerSeason, int bowlersToCut) {
		this.game = game;
		this.seasonID = seasonID;
		
		this.weeksPerSeason = weeksPerSeason;
		this.bowlersToCut = bowlersToCut;
		this.bowlersPerSeason = bowlersPerSeason;
		
		activeDay = new Day(1, bowlersPerSeason);
		days.add(activeDay);
	}
	
	public int seasonID() {
		return seasonID;
	}
	
	public void addBowler(Bowler bowlerID) {
		bowlers.add(bowlerID);
	}
	
	public List<Bowler> bowlers() {
		return Collections.unmodifiableList(bowlers);
	}
	
	public List<Bowler> cutBowlers() {
		List<BowlerSeasonStats> stats = getStats(days);
		Collections.reverse(stats);
		
		return stats.stream()
				.limit(bowlersToCut)
				.map(stat -> stat.bowlerID)
				.collect(Collectors.toList());
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Season season = (Season) o;
		return seasonID == season.seasonID &&
				Objects.equals(bowlers, season.bowlers) &&
				Objects.equals(days, season.days) &&
				Objects.equals(activeDay, season.activeDay);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(seasonID, bowlers, days, activeDay);
	}
	
	@Override
	public String toString() {
		return "Season{" +
				"seasonID=" + seasonID +
				", bowlers=" + bowlers +
				", weeks=" + days +
				", activeWeek=" + activeDay +
				'}';
	}
	
	public Day activeWeek() {
		return activeDay;
	}
	
	public void addScore(Score score) {
		if (activeDay.isComplete() && isComplete()) {
			throw new RuntimeException("Season is already complete. Cannot add new score.");
		}
		activeDay.add(score);
		
		if (activeDay.isComplete() && !isComplete()) {
			activeDay.scores().get(0).getBowler().dayWins++;

			activeDay = new Day(activeDay.weekID() + 1, bowlersPerSeason);
			days.add(activeDay);
		}
	}
	
	public List<Day> weeks() {
		return Collections.unmodifiableList(days);
	}
	
	public boolean isComplete() {
		return days.size() >= weeksPerSeason && activeDay.isComplete();
	}
	
	public String describe() {
		StringBuilder builder = new StringBuilder(String.format("Season %d Standings, Week %d / %d", seasonID, activeWeek().weekID() - 1, weeksPerSeason));
		builder.append("\n");
		
		ConsoleTable table = new ConsoleTable();
		table.setHeaders("Pos", "Bowler", "Points", "Bonus", "W", "S", "High", "Best", "Wins", "Notes");
		table.setColumnAlignment(RIGHT, RIGHT, RIGHT, RIGHT, RIGHT, RIGHT, RIGHT, RIGHT, RIGHT);
		
		List<BowlerSeasonStats> stats = getStats(days);
		int limit = days.size() - (days.get(days.size() - 1).isComplete() ? 1 : 2);
		if (limit < 0) {
			limit = 0;
		}
		List<BowlerSeasonStats> prevStats = getStats(days.stream().limit(limit).collect(Collectors.toList()));
		
		int i = 1;
		
		for (BowlerSeasonStats bowlerSeasonStat : stats) {
			int prevPos = prevStats.indexOf(prevStats.stream()
					.filter(stat -> stat.bowlerID == bowlerSeasonStat.bowlerID)
					.findFirst().get()) + 1;
			int weekChange = limit > 0 ? prevPos - i : 0;
			int seasonChange = bowlerSeasonStat.startPos - i;

			String notes = "";

			if (game.previousSeasonWinner() == bowlerSeasonStat.bowlerID.id) {
				notes += "Champion";
			}

			if ((!isComplete() && bowlerSeasonStat.bowlerID.totalSeasons == 0) ||
					(isComplete() && bowlerSeasonStat.bowlerID.totalSeasons == 1)) {
				if (!notes.isEmpty()) { notes += ", "; }
				notes += "New Bowler";
			}
			else if (!isComplete() && bowlerSeasonStat.bowlerID.totalSeasons + 1 >= game.getSeasonsTillRetirement()) {
				if (!notes.isEmpty()) { notes += ", "; }
				notes += "Final Season";
			}
			else if (isComplete() && bowlerSeasonStat.bowlerID.totalSeasons >= game.getSeasonsTillRetirement()) {
				if (!notes.isEmpty()) { notes += ", "; }
				notes += "Final Season";
			}
			if (i > (bowlersPerSeason - bowlersToCut))
			{
				if (!notes.isEmpty()) { notes += ", "; }
				notes += "In Cut";
			}
			table.addRow(
					String.format("#%2d#", i),
					String.valueOf(bowlerSeasonStat.bowlerID.id),
					String.valueOf(bowlerSeasonStat.points),
					bowlerSeasonStat.bonusPoints == 0 ? "" : String.valueOf(bowlerSeasonStat.bonusPoints),
					weekChange == 0 ? "--" : (weekChange > 0 ? "+" : "") + weekChange,
					seasonChange == 0 ? "--" : (seasonChange > 0 ? "+" : "") + seasonChange,
					String.valueOf(bowlerSeasonStat.highGame),
					String.valueOf(bowlerSeasonStat.bestPosition),
					String.valueOf(bowlerSeasonStat.wins),
					notes
			);
			i++;
		}
		
		builder.append(table.print());
		
		return builder.toString();
	}
	
	public List<BowlerSeasonStats> getStats() {
		return getStats(weeks());
	}
	
	private List<BowlerSeasonStats> getStats(List<Day> days) {
		List<BowlerSeasonStats> bowlerSeasonStats = calcStats(days);
		
		List<BowlerSeasonStats> highGames = calcHighGames(days);
		
		for (BowlerSeasonStats highGame : highGames) {
			Optional<BowlerSeasonStats> first = bowlerSeasonStats.stream()
					.filter(stats -> stats.bowlerID == highGame.bowlerID)
					.findFirst();
			
			if (first.isPresent()) {
				first.get().points += highGame.bonusPoints;
				first.get().bonusPoints += highGame.bonusPoints;
			}
		}
		
		bowlerSeasonStats.sort(((Comparator<BowlerSeasonStats>) (o1, o2) -> {
			if (o1.points == o2.points) {
				if (o1.highGame == o2.highGame) {
					if (o1.bestPosition == o2.bestPosition) {
						return Integer.compare(o1.wins, o2.wins);
					}
					return Integer.compare(o2.bestPosition, o1.bestPosition);
				}
				return Integer.compare(o1.highGame, o2.highGame);
			}
			return Integer.compare(o1.points, o2.points);
		}).reversed());
		return bowlerSeasonStats;
	}
	
	public static class BowlerSeasonStats {
		public final Bowler bowlerID;
		int points;
		int bonusPoints;
		int highGame;
		int bestPosition;
		int wins;
//		int prevWeekPos;
		int startPos;
		
		public BowlerSeasonStats(Bowler bowlerID) {
			this.bowlerID = bowlerID;
		}
	}
	
	private List<BowlerSeasonStats> calcHighGames(List<Day> days) {
		Map<Bowler, BowlerSeasonStats> highGames = new HashMap<>();
		
		for (Bowler bowlerID : bowlers) {
			BowlerSeasonStats stats = new BowlerSeasonStats(bowlerID);
			stats.highGame = 0;
			highGames.put(bowlerID, stats);
		}
		
		for (Day day : days) {
			if (!day.isComplete()) {
				continue;
			}
			for (Bowler bowlerID : bowlers) {
				BowlerSeasonStats stats = highGames.get(bowlerID);
				int score = day.scoreForBowler(bowlerID);
				
				if (score > stats.highGame) {
					stats.highGame = score;
				}
				highGames.put(bowlerID, stats);
			}
		}
		
		List<BowlerSeasonStats> collect = highGames.values().stream()
				.sorted((o1, o2) -> Integer.compare(o2.highGame, o1.highGame)) // reversed
				.collect(Collectors.toList());
		
		int bonus = 3;
		int limit = 0;
		
		for (int i = 0; i < collect.size(); i++) {
			BowlerSeasonStats stats = collect.get(i);
			stats.bonusPoints = bonus;
			limit++;
			
			if (i + 1 < collect.size() && collect.get(i + 1).highGame < stats.highGame) {
				bonus--;
			}
			
			if (bonus == 0) {
				break;
			}
		}
		
		return collect.stream()
				.limit(limit)
				.collect(Collectors.toList());
	}
	
	private List<BowlerSeasonStats> calcStats(List<Day> days) {
		List<BowlerSeasonStats> stats = new ArrayList<>();
		
		for (Bowler bowlerID : bowlers) {
			stats.add(statsForBowler(bowlerID, days));
		}
		
		return stats;
	}
	
	private BowlerSeasonStats statsForBowler(Bowler bowlerID, List<Day> days) {
		BowlerSeasonStats stats = new BowlerSeasonStats(bowlerID);
		stats.highGame = 0;
		stats.points = 0;
		stats.bonusPoints = 0;
		stats.bestPosition = 0;
		stats.wins = 0;
		
		for (int i = 0; i < days.size(); i++) {
			Day day = days.get(i);
			
			if (!day.isComplete()) {
				continue;
			}
			
			int pos = day.positionForBowler(bowlerID);
			int game = day.scoreForBowler(bowlerID);
			int points = day.pointsForBowler(bowlerID);
			
			if (i == 0) {
				stats.startPos = pos;
			}
			
			if (game > stats.highGame) {
				stats.highGame = game;
			}
			
			if (pos == 1) {
				stats.wins++;
			}
			
			if (pos < stats.bestPosition || stats.bestPosition == 0) {
				stats.bestPosition = pos;
			}
			
			if (game == 300) {
				stats.bonusPoints += Game.PERFECT_GAME_BONUS_POINTS;
				stats.points += Game.PERFECT_GAME_BONUS_POINTS;
			}
			
			stats.points += points;
		}
		
		return stats;
	}
}
