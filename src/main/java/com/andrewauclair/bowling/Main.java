package com.andrewauclair.bowling;

import com.andrewauclair.bowling.ui.MainFrame;

import javax.swing.*;
import java.io.IOException;
import java.util.*;

public class Main {
	public static void main(String[] args) throws IOException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		
		Game game = new Game(Game.DEFAULT_BOWLERS_PER_SEASON, Game.DEFAULT_WEEKS_PER_SEASON, Game.DEFAULT_RESERVE_BOWLERS, Game.DEFAULT_BOWLERS_TO_CUT, Game.DEFAULT_WAIT_SEASON_COUNT, Game.DEFAULT_SEASONS_TILL_RETIREMENT);
		
		System.out.println("user: " + System.getenv("username"));
		
		GregorianCalendar calendar = new GregorianCalendar(1958, Calendar.JANUARY, 5);
		
		List<Bowler> bowlers = new ArrayList<>(game.reserveBowlers());
		List<Bowler> remaining = new ArrayList<>(bowlers);
		
		Collections.shuffle(remaining);
		
		Game.DataLoader dataLoader = new Game.DataLoader(game, calendar).invoke(bowlers, remaining);
		
		MainFrame frame = new MainFrame(game);
		frame.setVisible(true);
	}
}
