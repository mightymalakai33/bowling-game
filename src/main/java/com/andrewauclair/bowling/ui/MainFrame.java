package com.andrewauclair.bowling.ui;

import com.andrewauclair.bowling.MenuBar;
import com.andrewauclair.bowling.*;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;
import java.util.*;

public class MainFrame extends JFrame {
	private final Game game;
	
	public MainFrame(Game game) {
		this.game = game;
		
		// menu bar: file (exit), season (standings, days, bowlers)
		setJMenuBar(new MenuBar());
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		setMinimumSize(new Dimension(200, 200));
		
		JTable table = new JTable(new DataTableModel(collectStats()));
		table.setAutoCreateRowSorter(true);
		
		add(new JScrollPane(table));
		
		pack();
	}
	
	private static class DataTableModel extends AbstractTableModel {
		List<BowlerStats> data = new ArrayList<>();
		final List<String> columns = Arrays.asList(
				"Bowler ID", "Comps", "Seasons", "Comp Wins", "Season Wins", "Day Wins", "Eliminations", "High Game", "Total Pins", "Total Games", "Average"
		);
		final List<Class<?>> columnClasses = Arrays.asList(
				Integer.class, Integer.class, Integer.class, Integer.class, Integer.class, Integer.class, Integer.class, Integer.class, Integer.class, Integer.class,
				Double.class
		);
		
		public DataTableModel(List<BowlerStats> data) {
			this.data = data;
		}
		
		@Override
		public int getRowCount() {
			return data.size();
		}
		
		@Override
		public int getColumnCount() {
			return columns.size();
		}
		
		@Override
		public String getColumnName(int column) {
			return columns.get(column);
		}
		
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			return columnClasses.get(columnIndex);
		}
		
		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			BowlerStats stat = data.get(rowIndex);
			
			switch (columnIndex) {
			case 0: // bowler ID
				return stat.bowler.id;
			case 1: // comps
				return stat.comps;
			case 2: // seasons
				return stat.seasons;
			case 3: // comp wins
				return stat.compWins;
			case 4: // season wins
				return stat.seasonWins;
			case 5: // day wins
				return stat.dayWins;
			case 6: // eliminations
				return stat.eliminations;
			case 7: // high game
				return stat.highGame;
			case 8: // total pins
				return stat.totalPins;
			case 9: // total games
				return stat.totalGames;
			case 10: // average
				if (stat.totalGames == 0) {
					return 0.0;
				}
				return stat.totalPins / (double) stat.totalGames;
			}
			return null;
		}
	}
	
	private static class BowlerStats {
		final Bowler bowler;
		int comps;
		int seasons;
		int seasonWins;
		int dayWins;
		int compWins;
		int eliminations;
		int highGame;
		int totalPins;
		int totalGames;
		
		public BowlerStats(Bowler bowler) {
			this.bowler = bowler;
		}
	}
	
	List<BowlerStats> collectStats() {
		Map<Bowler, BowlerStats> stats = new HashMap<>();
		
		for (Comp comp : game.comps()) {
			for (Comp.Score score : comp.getFinalScores()) {
				BowlerStats stat = stats.getOrDefault(score.bowler, new BowlerStats(score.bowler));
				
				stat.comps++;
				
				if (comp.isComplete() && comp.getBowlers().contains(score.bowler)) {
					stat.compWins++;
				}
				
				stats.put(score.bowler, stat);
			}
		}
		
		for (Season season : game.seasons()) {
			for (Day week : season.weeks()) {
				for (Score score : week.scores()) {
					BowlerStats stat = stats.getOrDefault(score.getBowler(), new BowlerStats(score.getBowler()));
					
					stat.totalGames++;
					stat.totalPins += score.score();
					
					if (week.isComplete() && week.positionForBowler(score.getBowler()) == 1) {
						stat.dayWins++;
					}
					
					if (score.score() > stat.highGame) {
						stat.highGame = score.score();
					}
					
					stats.put(score.getBowler(), stat);
				}
			}
			
			for (Bowler bowlerID : season.bowlers()) {
				BowlerStats stat = stats.getOrDefault(bowlerID, new BowlerStats(bowlerID));
				
				stat.seasons++;
				
				stats.put(bowlerID, stat);
			}
			
			if (season.isComplete()) {
				List<Season.BowlerSeasonStats> seasonStats = season.getStats();
				
				Season.BowlerSeasonStats winner = seasonStats.get(0);
				
				List<Bowler> cutBowlers = season.cutBowlers();
				
				BowlerStats stat = stats.getOrDefault(winner.bowlerID, new BowlerStats(winner.bowlerID));
				stat.seasonWins++;
				stats.put(winner.bowlerID, stat);
				
				for (Bowler cutBowler : cutBowlers) {
					BowlerStats cutStat = stats.getOrDefault(cutBowler, new BowlerStats(cutBowler));
					cutStat.eliminations++;
					stats.put(cutBowler, cutStat);
				}
			}
		}
		return new ArrayList<>(stats.values());
	}
}
