package com.andrewauclair.bowling;

import java.util.*;

public class Day {
	private final List<Score> scores = new ArrayList<>();
	private final int weekID;
	
	private final int bowlersPerSeason;
	
	public Day(int weekID, int bowlersPerSeason) {
		this.weekID = weekID;
		this.bowlersPerSeason = bowlersPerSeason;
	}
	
	public int weekID() {
		return weekID;
	}
	
	public void add(Score score) {
		if (isComplete()) {
			throw new RuntimeException("Week is already complete. Cannot add new score.");
		}
		scores.add(score);
	}
	
	public List<Score> scores() {
		List<Score> scores = new ArrayList<>(this.scores);
		
		scores.sort(((Comparator<Score>) (o1, o2) -> {
			return Integer.compare(o1.score(), o2.score());
		}).reversed());
		
		return scores;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Day day = (Day) o;
		return weekID == day.weekID &&
				Objects.equals(scores, day.scores);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(scores, weekID);
	}
	
	@Override
	public String toString() {
		return "Week{" +
				"scores=" + scores +
				", weekID=" + weekID +
				'}';
	}
	
	public boolean isComplete() {
		return scores.size() >= bowlersPerSeason;
	}
	
	public String description(int seasonID) {
		StringBuilder builder = new StringBuilder(String.format("Season %d, Week %d", seasonID, weekID));
		
		builder.append("\n");
		builder.append("\n");
		
		for (Score score : scores()) {
			builder.append(String.format("%2d", score.getBowler().id));
			builder.append("  ");
			builder.append(score.score());
			builder.append("\n");
		}
		builder.append("\n");
		
		return builder.toString();
	}
	
	public int scoreForBowler(Bowler bowlerID) {
		Optional<Score> first = scores.stream()
				.filter(score -> score.getBowler().id == bowlerID.id)
				.findFirst();
		
		return first.map(Score::score).orElse(0);
	}
	
	public int pointsForBowler(Bowler bowlerID) {
		return bowlersPerSeason - positionForBowler(bowlerID) + 1;
	}
	
	public int positionForBowler(Bowler bowlerID) {
		int pos = 1;
		List<Score> sortedScores = scores();
		
		for (int i = 0; i < sortedScores.size(); i++) {
			if (sortedScores.get(i).getBowler().id == bowlerID.id) {
				return pos;
			}
			
			if (i + 1 < sortedScores.size() && sortedScores.get(i + 1).score() < sortedScores.get(i).score()) {
				pos = i + 2;
			}
		}
		return 0;
	}
}
