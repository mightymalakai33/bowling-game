package com.andrewauclair.bowling;

public class Bowler {
	public final int id;

	public int totalSeasons = 0;
	public int dayWins = 0;
	public int seasonWins = 0;

	public Bowler(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return String.valueOf(id);
	}
}
