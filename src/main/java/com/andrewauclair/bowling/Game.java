package com.andrewauclair.bowling;

import com.andrewauclair.ui.MainFrame;

import javax.swing.*;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Game {
	public static final int DEFAULT_BOWLERS_PER_SEASON = 10;
	public static final int DEFAULT_WEEKS_PER_SEASON = 10;
	public static final int DEFAULT_RESERVE_BOWLERS = 50;
	
	public static final int DEFAULT_BOWLERS_TO_CUT = 3;
	public static final int DEFAULT_WAIT_SEASON_COUNT = 6;
	
	public static final int PERFECT_GAME_BONUS_POINTS = 3;

	public static final int DEFAULT_SEASONS_TILL_RETIREMENT = 20;

	private final List<Bowler> activeBowlers = new ArrayList<>();
	private final List<Bowler> reserveBowlers = new ArrayList<>();
	private final Map<Bowler, Integer> waitingBowlers = new HashMap<>();
	private final List<Bowler> retiredBowlers = new ArrayList<>();

	private int nextBowler; // next bowler to enter after a bowler retires

	private final List<Comp> comps = new ArrayList<>();
	private final List<Season> seasons = new ArrayList<>();
	
	private Comp competition;
	private Season activeSeason;
	
	private final int bowlersPerSeason;
	private final int weeksPerSeason;
	private final int reserveBowlersCount;
	private final int bowlersToCut;
	private final int waitSeasonCount;

	public int getSeasonsTillRetirement() {
		return seasonsTillRetirement;
	}

	private final int seasonsTillRetirement;
	
	public Game(int bowlersPerSeason, int weeksPerSeason, int reserveBowlersCount, int bowlersToCut, int waitSeasonCount, int seasonsTillRetirement) {
		this.bowlersPerSeason = bowlersPerSeason;
		this.weeksPerSeason = weeksPerSeason;
		this.reserveBowlersCount = reserveBowlersCount;
		this.bowlersToCut = bowlersToCut;
		this.waitSeasonCount = waitSeasonCount;
		this.seasonsTillRetirement = seasonsTillRetirement;

		List<Integer> collect = IntStream.range(1, reserveBowlersCount + bowlersPerSeason + 1).boxed().collect(Collectors.toList());

		for (Integer integer : collect) {
			this.reserveBowlers.add(new Bowler(integer));
		}

		nextBowler = reserveBowlersCount + bowlersPerSeason + 1;

		competition = new Comp(bowlersPerSeason, this.reserveBowlers);
		comps.add(competition);
		
		activeSeason = new Season(this, 0, 0, 0, 0);
	}
	
	public List<Bowler> bowlers() {
		return Collections.unmodifiableList(activeBowlers);
	}
	
	public List<Bowler> reserveBowlers() {
		return Collections.unmodifiableList(reserveBowlers);
	}
	
	public Map<Bowler, Integer> waitingBowlers() {
		return Collections.unmodifiableMap(waitingBowlers);
	}
	
	// used in the tests as a shortcut so we don't have to build up so much stuff
	public void setWaitingBowler(Bowler bowlerID, int seasons) {
		waitingBowlers.put(bowlerID, seasons);
	}
	
	public Season activeSeason() {
		return activeSeason;
	}

	public int previousSeasonWinner() {
		if (activeSeason.seasonID() > 1) {
			return seasons.get(activeSeason.seasonID() - 1).bowlers().get(0).id;
		}
		return 0;
	}
	
	public void addScore(Bowler bowlerID, int score) {
		if (!competition.isComplete()) {
			competition.addScore(bowlerID, score);
			
			if (competition.isComplete()) {
				activeSeason = new Season(this, activeSeason.seasonID() + 1, bowlersPerSeason, weeksPerSeason, bowlersToCut);
				seasons.add(activeSeason);
				
				activeBowlers.addAll(competition.getBowlers());
				reserveBowlers.removeAll(competition.getBowlers());
				
				for (Bowler bowler : activeBowlers) {
					activeSeason.addBowler(bowler);
				}
			}
		}
		else {
			activeSeason.addScore(new Score(bowlerID, score));
			
			if (activeSeason.isComplete()) {
				activeSeason.bowlers().get(0).seasonWins++;

				for (Bowler bowler : activeSeason.bowlers()) {
					bowler.totalSeasons++;

					if (bowler.totalSeasons >= seasonsTillRetirement) {
						System.out.println(String.format("Bowler " + bowler.id + " has retired with %d wins and %d championships.", bowler.dayWins, bowler.seasonWins));

						System.out.println("Bowler " + nextBowler + " has joined.");

						activeBowlers.removeIf(b -> b.id == bowler.id);
						reserveBowlers.add(new Bowler(nextBowler));
						retiredBowlers.add(bowler);
						nextBowler++;
					}
				}

				// decrease wait times, bring back bowlers that are done waiting
				List<Bowler> bowlersBroughtBack = new ArrayList<>();
				for (Bowler bowler : waitingBowlers.keySet()) {
					int seasonsLeft = waitingBowlers.get(bowler);
					if (seasonsLeft <= 1) {
						reserveBowlers.add(bowler);
//						waitingBowlers.remove(bowler);
						bowlersBroughtBack.add(bowler);
					}
					else {
						waitingBowlers.put(bowler, seasonsLeft - 1);
					}
				}
				
				for (Bowler bowler : bowlersBroughtBack) {
					waitingBowlers.remove(bowler);
				}

				// move the 3 lowest bowlers to the timeout map
				for (Bowler bowler : activeSeason.cutBowlers()) {
					waitingBowlers.put(bowler, waitSeasonCount);
					activeBowlers.removeIf(b -> b.id == bowler.id);
				}

				// start next competition
				competition = new Comp(bowlersPerSeason - activeBowlers.size(), reserveBowlers);
				comps.add(competition);
			}
		}
	}
	
	public List<Comp> comps() {
		return Collections.unmodifiableList(comps);
	}
	
	public List<Season> seasons() {
		return Collections.unmodifiableList(seasons);
	}
	
	private static final String folder = "C:\\Users\\" + System.getenv("username") + "\\Dropbox\\wii-bowling-game\\";

	public static int readNumber(BufferedReader reader) {
		String input;
		boolean invalid_input = true;
		int value = 0;

		do {
			try {
				input = reader.readLine();
				value = Integer.parseInt(input);
				invalid_input = false;
			}
			catch (IOException | NumberFormatException e) {
				e.printStackTrace();
			}
		} while (invalid_input);

		return value;
	}

	public static int readNumber(BufferedReader reader, int defaultValue) {
		String input;
		
		try {
			input = reader.readLine();
			return Integer.parseInt(input);
		}
		catch (IOException | NumberFormatException ignored) {
		}
		return defaultValue;
	}
	
	public static void main(String[] args) throws IOException {
		String input = "";
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Preset #1 - bowlers: 10, weeks: 10, reserve bowlers: 50, bowlers to cut: 3, wait seasons: 6, seasons to retire: 20");
		System.out.println("Preset #2 - bowlers: 2, weeks: 5, reserve bowlers: 16, bowlers to cut: 1, wait seasons: 6, seasons to retire: 10");

		System.out.print("Preset selection: ");
		int preset = readNumber(reader, 0);

		int bowlersPerSeason = Game.DEFAULT_BOWLERS_PER_SEASON;
		int weeksPerSeason = Game.DEFAULT_WEEKS_PER_SEASON;
		int reserveBowlers = Game.DEFAULT_RESERVE_BOWLERS;
		int bowlersToCut = Game.DEFAULT_BOWLERS_TO_CUT;
		int waitSeasonCount = Game.DEFAULT_WAIT_SEASON_COUNT;
		int seasonsTillRetirement = Game.DEFAULT_SEASONS_TILL_RETIREMENT;

		if (preset == 0) {
			System.out.print("Bowlers per season: ");
			bowlersPerSeason = readNumber(reader);
			System.out.print("Weeks per season: ");
			weeksPerSeason = readNumber(reader);
			System.out.print("Reserve bowlers: ");
			reserveBowlers = readNumber(reader);
			System.out.print("Bowlers to cut: ");
			bowlersToCut = readNumber(reader);
			System.out.print("Wait season count: ");
			waitSeasonCount = readNumber(reader);
			System.out.print("Seasons till retirement: ");
			seasonsTillRetirement = readNumber(reader);
		}
		else if (preset == 1) {
			// preset 1 is the current defaults
		}
		else if (preset == 2) {
			bowlersPerSeason = 2;
			weeksPerSeason = 5;
			reserveBowlers = 16;
			bowlersToCut = 1;
			waitSeasonCount = 6;
			seasonsTillRetirement = 10;
		}

//		System.out.print("Step through each season: ");
//		boolean stepThroughSeasons = readNumber(reader, 0) != 0;
		
		Game game = new Game(bowlersPerSeason, weeksPerSeason, reserveBowlers, bowlersToCut, waitSeasonCount, seasonsTillRetirement);

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

			MainFrame frame = new MainFrame(game);
			frame.setSize(500, 500);

			frame.setVisible(true);
		}
		catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		System.out.println("user: " + System.getenv("username"));
		
		GregorianCalendar calendar = new GregorianCalendar(1958, Calendar.JANUARY, 5);
		
		List<Bowler> bowlers = new ArrayList<>(game.reserveBowlers());
		List<Bowler> remaining = new ArrayList<>(bowlers);
		
		Collections.shuffle(remaining);
		
		DataLoader dataLoader = new DataLoader(game, calendar).invoke(bowlers, remaining);
		bowlers = dataLoader.getBowlers();
		remaining = dataLoader.getRemaining();
		
		do {
			Bowler next_bowler = remaining.get(0);
			System.out.printf("score for bowler %d (seasons: %d, wins: %d, champs: %d): ", next_bowler.id, next_bowler.totalSeasons, next_bowler.dayWins, next_bowler.seasonWins);
			input = reader.readLine();
			
			int score;
			try {
				score = Integer.parseInt(input.trim());
			}
			catch (Exception e) {
				continue;
			}
			
			AddGame addGame = new AddGame(game, calendar, bowlers, remaining, score).invoke(true);
			bowlers = addGame.getBowlers();
			remaining = addGame.getRemaining();
			
			if (addGame.shouldResetReader()) {
				System.in.read(new byte[System.in.available()]);
				reader = new BufferedReader(new InputStreamReader(System.in));
				
				dataLoader.invoke(bowlers, remaining);
				bowlers = dataLoader.getBowlers();
				remaining = dataLoader.getRemaining();
			}
		}
		while (!input.equals("exit"));
	}
	
	public boolean isCompetition() {
		return !competition.isComplete();
	}
	
	public Comp getCompetition() {
		return competition;
	}
	
	private static class AddGame {
		private final Game game;
		private final GregorianCalendar calendar;
		private List<Bowler> bowlers;
		private List<Bowler> remaining;
		private final int score;
		private boolean resetReader;
		
		public AddGame(Game game, GregorianCalendar calendar, List<Bowler> bowlers, List<Bowler> remaining, int score) {
			this.game = game;
			this.calendar = calendar;
			this.bowlers = bowlers;
			this.remaining = remaining;
			this.score = score;
			this.resetReader = false;
		}
		
		public List<Bowler> getBowlers() {
			return bowlers;
		}
		
		public List<Bowler> getRemaining() {
			return remaining;
		}
		
		public boolean shouldResetReader() {
			return resetReader;
		}
		
		public AddGame invoke(boolean addToFile) throws IOException {
			if (!game.isCompetition()) {
				Season season = game.activeSeason();
				Day day = season.activeWeek();

				Bowler bowlerID = remaining.remove(0);
				
				if (addToFile) {
					try (BufferedWriter fileOut = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(folder + "seasons.txt"), true)))) {
						fileOut.write(score + "\n");
					}
//					try (BufferedWriter fileOut = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(folder + "season" + game.activeSeason().seasonID() + ".txt"), true)))) {
//						fileOut.write(score + "\n");
//					}
				}
				
				game.addScore(bowlerID, score);
				
				if (addToFile) {
					System.out.println("bowler: " + bowlerID + " score: " + score + "    remaining: " + remaining.size());
				}
				
				if (remaining.size() == 0) {
					if (addToFile) {
						System.out.println();
						System.out.println(day.description(season.seasonID()));
						
						System.out.println();
						System.out.println(season.describe());
					}
					
					if (season.isComplete()) {
						resetReader = true;
						
						bowlers = new ArrayList<>(game.reserveBowlers());
						
						calendar.add(Calendar.DAY_OF_MONTH, 2);
						
						System.out.println("Season " + season.seasonID() + " complete.");
						System.out.println();
						System.out.println(season.describe());
						System.out.println("Bowler waiting list");
						
						ConsoleTable table = new ConsoleTable();
						table.setHeaders("Bowler", "Seasons");
						
						game.waitingBowlers.entrySet().stream()
								.sorted(((Comparator<Map.Entry<Bowler, Integer>>) (o1, o2) -> {
									return Integer.compare(o1.getValue(), o2.getValue());
								}).reversed()).forEach(val -> {
							table.addRow(String.valueOf(val.getKey()), String.valueOf(val.getValue()));
						});
						
						System.out.print(table.print());
						System.out.println();
						
						if (addToFile) {
							Comp competition = game.getCompetition();
							
							printDate(calendar);
							System.out.println("Starting competition (" + competition.cutSize() + ", " + competition.getBowlers().size() + ").");
							System.out.println("Reserve bowlers (" + bowlers.size() + "): " + bowlers);
						}
					}
					else {
						calendar.add(Calendar.DAY_OF_MONTH, 1);
						
						if (season.activeWeek().weekID() == 6) {
							calendar.add(Calendar.DAY_OF_MONTH, 2);
						}
						
						if (addToFile) {
							printDate(calendar);
						}
					}
					remaining = new ArrayList<>(bowlers);
//					Collections.shuffle(remaining);
					
					List<Bowler> orderInStats = new ArrayList<>();
					
					for (Season.BowlerSeasonStats stat : season.getStats()) {
						orderInStats.add(stat.bowlerID);
					}
					
					remaining.sort(Comparator.comparingInt(o -> o.id));
				}
			}
			else {
				Bowler bowlerID = remaining.remove(0);
				
				if (addToFile) {
					try (BufferedWriter fileOut = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(folder + "comps.txt"), true)))) {
						fileOut.write(score + "\n");
					}
//					try (BufferedWriter fileOut = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(folder + "comp" + (game.activeSeason().seasonID() + 1) + ".txt"), true)))) {
//						fileOut.write(score + "\n");
//					}
				}
				
				game.addScore(bowlerID, score);
				
				if (addToFile) {
					System.out.println("bowler: " + bowlerID.id + " score: " + score + "    remaining: " + remaining.size());
				}
				
				Comp competition = game.getCompetition();
				
				if (remaining.size() == 0 && !competition.isComplete()) {
					if (addToFile) {
						System.out.println("Competition not complete.");
					}
					bowlers = new ArrayList<>(competition.getBowlers());
					remaining = new ArrayList<>(bowlers);
					if (addToFile) {
						System.out.println("Locked-In: (" + competition.lockedInBowlers().size() + "): " + competition.lockedInBowlers());
						System.out.println("Remaining (" + competition.getBowlers().size() + "): " + competition.getBowlers());
					}
					Collections.shuffle(remaining);
				}
				else if (competition.isComplete()) {
					resetReader = true;
					
//					if (addToFile) {
						System.out.println("Competition (" + competition.cutSize() + ", " + competition.totalBowlers() + ") complete.");
//					}
					
					bowlers = new ArrayList<>(game.bowlers());
					remaining = new ArrayList<>(bowlers);
					
					calendar.add(Calendar.DAY_OF_MONTH, 1);
					
					if (addToFile) {
						printDate(calendar);
						System.out.println("Starting season " + game.activeSeason().seasonID() + " with bowlers: " + bowlers);
					}
					Collections.shuffle(remaining);
				}
			}
			return this;
		}
	}
	
	private static void printDate(GregorianCalendar calendar) {
		System.out.println((calendar.get(Calendar.MONTH) + 1) + " / " + calendar.get(Calendar.DAY_OF_MONTH) + " / " + calendar.get(Calendar.YEAR));
	}
	
	public static class DataLoader {
		private final Game game;
		private final GregorianCalendar calendar;
		private List<Bowler> bowlers;
		private List<Bowler> remaining;

		private final List<Integer> competitionScores = new ArrayList<>();
		private final List<Integer> gameScores = new ArrayList<>();

		public DataLoader(Game game, GregorianCalendar calendar) {
			this.game = game;
			this.calendar = calendar;

			try {
				File comps = new File(folder + "comps.txt");
				BufferedReader compsIn = new BufferedReader(new InputStreamReader(new FileInputStream(comps)));

				File seasons = new File(folder + "seasons.txt");
				BufferedReader seasonsIn = new BufferedReader(new InputStreamReader(new FileInputStream(seasons)));

				String str;

				do {
					str = compsIn.readLine();
					if (str != null) {
						competitionScores.add(Integer.parseInt(str.trim()));
					}
				} while (str != null);

				do {
					str = seasonsIn.readLine();
					if (str != null) {
						gameScores.add(Integer.parseInt(str.trim()));
					}
				} while (str != null);
			}
			catch (IOException e) {
				e.printStackTrace();
			}


		}
		
		public List<Bowler> getBowlers() {
			return bowlers;
		}
		
		public List<Bowler> getRemaining() {
			return remaining;
		}
		
		public DataLoader invoke(List<Bowler> origBowlers, List<Bowler> bowlersRemaining) {
			this.bowlers = origBowlers;
			this.remaining = bowlersRemaining;
			
			while (true) {
//				String input = game.isCompetition() ? compsIn.readLine() : seasonsIn.readLine();
				
				boolean isComp = game.isCompetition();

				if (isComp && competitionScores.size() == 0) {
					System.out.println("No more competition scores in file. Game scores remaining: " + gameScores.size());
					break;
				}
				else if (!isComp && gameScores.size() == 0) {
					System.out.println("No more game scores in file. Competition scores remaining: " + competitionScores.size());
					break;
				}
				
				try {
					int score = isComp ? competitionScores.remove(0) : gameScores.remove(0);//Integer.parseInt(input.trim());

					AddGame addGame = new AddGame(game, calendar, this.bowlers, this.remaining, score).invoke(false);
					this.bowlers = addGame.getBowlers();
					this.remaining = addGame.getRemaining();

					if (isComp != game.isCompetition()) {
//						System.in.read();
					}
//						if (addGame.shouldResetReader()) {
//							break;
//						}
				}
				catch (Exception ignored) {
					ignored.printStackTrace();
				}
			}
			
//
//			String input;
//			while (new File(folder + "comp" + (game.activeSeason().seasonID() + 1) + ".txt").exists()) {
//				try (BufferedReader fileIn = new BufferedReader(new InputStreamReader(new FileInputStream(new File(folder + "comp" + (game.activeSeason().seasonID() + 1) + ".txt"))))) {
//					input = fileIn.readLine();
//
//					while (input != null) {
//						int score;
//						try {
//							score = Integer.parseInt(input.trim());
//
//							AddGame addGame = new AddGame(game, calendar, bowlers, remaining, score).invoke(false);
//							bowlers = addGame.getBowlers();
//							remaining = addGame.getRemaining();
//
//							if (addGame.shouldResetReader()) {
//								break;
//							}
//						}
//						catch (Exception ignored) {
//						}
//						finally {
//							input = fileIn.readLine();
//						}
//					}
//				}
//
//				if (!game.isCompetition() && new File(folder + "season" + game.activeSeason.seasonID() + ".txt").exists()) {
//					try (BufferedReader fileIn = new BufferedReader(new InputStreamReader(new FileInputStream(new File(folder + "season" + game.activeSeason().seasonID() + ".txt"))))) {
//						input = fileIn.readLine();
//
//						while (input != null) {
//							int score;
//							try {
//								score = Integer.parseInt(input.trim());
//
//								AddGame addGame = new AddGame(game, calendar, bowlers, remaining, score).invoke(false);
//								bowlers = addGame.getBowlers();
//								remaining = addGame.getRemaining();
//
//								if (addGame.shouldResetReader()) {
//									break;
//								}
//							}
//							catch (Exception ignored) {
//							}
//							finally {
//								input = fileIn.readLine();
//							}
//						}
//					}
//				}
//				else {
//					break;
//				}
//			}
			
			// tell the user what's going on
			if (game.isCompetition()) {
				Comp competition = game.getCompetition();
				
				printDate(calendar);
				
				System.out.println("Starting competition (cut: " + competition.cutSize() + ", comp bowlers: " + competition.getBowlers().size() + ").");
				System.out.println("Reserve bowlers (" + bowlers.size() + "): " + bowlers);
			}
			else {
				Season season = game.activeSeason();
				System.out.println(season.describe());
				System.out.println();
				printDate(calendar);
				
				List<Integer> orderInStats = new ArrayList<>();
				
				for (Season.BowlerSeasonStats stat : season.getStats()) {
					orderInStats.add(stat.bowlerID.id);
				}
				
				remaining.sort(Comparator.comparingInt(o -> o.id));
			}
			return this;
		}
	}
}
