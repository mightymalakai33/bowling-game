package com.andrewauclair.bowling;

import javax.swing.*;

public class MenuBar extends JMenuBar {
	private final JMenu file = new JMenu("File");
	private final JMenuItem exit = new JMenuItem("Exit");
	
	
	public MenuBar() {
		file.add(exit);
		
		add(file);
		
		exit.addActionListener(e -> System.exit(0));
	}
}
