package com.andrewauclair.bowling;

import java.util.Objects;

public final class Score {
	private final Bowler bowler;
	private final int score;
	
	public Score(Bowler bowler, int score) {
		this.bowler = bowler;
		this.score = score;
	}

	public Bowler getBowler() {
		return bowler;
	}

//	public int bowlerID() {
//		return bowlerID.id;
//	}
//
	public int score() {
		return score;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Score score1 = (Score) o;
		return bowler.id == score1.bowler.id &&
				score == score1.score;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(bowler.id, score);
	}
	
	@Override
	public String toString() {
		return "Score{" +
				"bowlerID=" + bowler.id +
				", score=" + score +
				'}';
	}
}
